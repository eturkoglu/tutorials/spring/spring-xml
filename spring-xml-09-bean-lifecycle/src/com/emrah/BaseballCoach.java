package com.emrah;

public class BaseballCoach implements Coach {

	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}
	
	public void initMethod() {
		System.out.println("Init method has been executed.");
	}

	public void destroyMethod() {
		System.out.println("Destroy method has been executed.");
	}
	
}
