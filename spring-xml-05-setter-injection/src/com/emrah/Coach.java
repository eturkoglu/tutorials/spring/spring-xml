package com.emrah;

public interface Coach {
	String getDailyWorkout();
	String getDailyFortune();
}
